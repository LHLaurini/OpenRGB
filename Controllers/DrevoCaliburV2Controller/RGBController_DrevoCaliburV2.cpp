/*----------------------------------------------*\
|  RGBController_DrevoCaliburV2.cpp              |
|                                                |
|  Generic RGB Interface for Drevo Calibur V2    |
|                                                |
|  Luiz Henrique Laurini (LHLaurini), 3/02/2024  |
\*----------------------------------------------*/

#include "RGBController_DrevoCaliburV2.h"
#include "DrevoCaliburV2Controller.h"
#include "RGBController.h"

#define NA                                 0xFFFFFFFF
#define DREVO_CALIBUR_V2_MATRIX_MAP_HEIGHT 5
#define DREVO_CALIBUR_V2_MATRIX_MAP_WIDTH  16
#define DREVO_CALIBUR_V2_MATRIX_CELL_COUNT (DREVO_CALIBUR_V2_MATRIX_MAP_HEIGHT * DREVO_CALIBUR_V2_MATRIX_MAP_WIDTH)
#define DREVO_CALIBUR_V2_NUM_KEYS          72

static unsigned int matrix_map[DREVO_CALIBUR_V2_MATRIX_MAP_HEIGHT][DREVO_CALIBUR_V2_MATRIX_MAP_WIDTH] =
    { {  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15 },
      { 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31 },
      { 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, NA, 45, 46 },
      { 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, NA, 59, 60, 61 },
      { 62, 63, 64, NA, NA, NA, 65, NA, NA, NA, 66, 67, 68, 69, 70, 71 } };

static unsigned int led_mappings[DREVO_CALIBUR_V2_NUM_KEYS] =
    { 19, 20,  21, 22, 23, 24, 25, 26, 27, 28, 29, 30,  31, 103, 116, 117,
      37, 38,  39, 40, 41, 42, 43, 44, 45, 46, 47, 48,  49,  85, 119, 120,
      55, 56,  57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 110,      118, 121,
      73, 111, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83,       84, 101, 115,
      91, 92,  93,             94,             95, 96,  98,  99, 100, 102 };

/**------------------------------------------------------------------*\
    @name Drevo Calibur V2
    @category Keyboard
    @type USB
    @save :robot:
    @direct :x:
    @effects :white_check_mark:
    @detectors DetectDrevoCaliburV2Controllers
    @comment
\*-------------------------------------------------------------------*/

RGBController_DrevoCaliburV2::RGBController_DrevoCaliburV2(DrevoCaliburV2Controller* controller_ptr)
{
    controller  = controller_ptr;

    vendor      = "Drevo";
    description = "Calibur V2";
    type        = DEVICE_TYPE_KEYBOARD;
    location    = controller->GetLocation();

    // TODO: improve naming (KeychronKeyboard should be a good template)

    mode Static;
    Static.name           = "Static";
    Static.value          = DREVO_CALIBUR_V2_MODE_STATIC;
    Static.flags          = MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_MODE_SPECIFIC_COLOR | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    Static.brightness_min = DREVO_CALIBUR_V2_BRIGHTNESS_MINIMUM;
    Static.brightness_max = DREVO_CALIBUR_V2_BRIGHTNESS_MAXIMUM;
    Static.colors_min     = 1;
    Static.colors_max     = 1;
    Static.brightness     = DREVO_CALIBUR_V2_BRIGHTNESS_DEFAULT;
    Static.color_mode     = MODE_COLORS_RANDOM;
    Static.colors.resize(1);
    modes.push_back(Static);

    mode Reactive;
    Reactive.name           = "Reactive";
    Reactive.value          = DREVO_CALIBUR_V2_MODE_REACTIVE;
    Reactive.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_MODE_SPECIFIC_COLOR | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    Reactive.speed_min      = DREVO_CALIBUR_V2_SPEED_MINIMUM;
    Reactive.speed_max      = DREVO_CALIBUR_V2_SPEED_MAXIMUM;
    Reactive.brightness_min = DREVO_CALIBUR_V2_BRIGHTNESS_MINIMUM;
    Reactive.brightness_max = DREVO_CALIBUR_V2_BRIGHTNESS_MAXIMUM;
    Reactive.colors_min     = 1;
    Reactive.colors_max     = 1;
    Reactive.speed          = DREVO_CALIBUR_V2_SPEED_DEFAULT;
    Reactive.brightness     = DREVO_CALIBUR_V2_BRIGHTNESS_DEFAULT;
    Reactive.color_mode     = MODE_COLORS_RANDOM;
    Reactive.colors.resize(1);
    modes.push_back(Reactive);

    mode ReactiveOff;
    ReactiveOff.name           = "Reactive off";
    ReactiveOff.value          = DREVO_CALIBUR_V2_MODE_REACTIVE_OFF;
    ReactiveOff.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_MODE_SPECIFIC_COLOR | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    ReactiveOff.speed_min      = DREVO_CALIBUR_V2_SPEED_MINIMUM;
    ReactiveOff.speed_max      = DREVO_CALIBUR_V2_SPEED_MAXIMUM;
    ReactiveOff.brightness_min = DREVO_CALIBUR_V2_BRIGHTNESS_MINIMUM;
    ReactiveOff.brightness_max = DREVO_CALIBUR_V2_BRIGHTNESS_MAXIMUM;
    ReactiveOff.colors_min     = 1;
    ReactiveOff.colors_max     = 1;
    ReactiveOff.speed          = DREVO_CALIBUR_V2_SPEED_DEFAULT;
    ReactiveOff.brightness     = DREVO_CALIBUR_V2_BRIGHTNESS_DEFAULT;
    ReactiveOff.color_mode     = MODE_COLORS_RANDOM;
    ReactiveOff.colors.resize(1);
    modes.push_back(ReactiveOff);

    mode Glittering;
    Glittering.name           = "Glittering";
    Glittering.value          = DREVO_CALIBUR_V2_MODE_GLITTERING;
    Glittering.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_MODE_SPECIFIC_COLOR | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    Glittering.speed_min      = DREVO_CALIBUR_V2_SPEED_MINIMUM;
    Glittering.speed_max      = DREVO_CALIBUR_V2_SPEED_MAXIMUM;
    Glittering.brightness_min = DREVO_CALIBUR_V2_BRIGHTNESS_MINIMUM;
    Glittering.brightness_max = DREVO_CALIBUR_V2_BRIGHTNESS_MAXIMUM;
    Glittering.colors_min     = 1;
    Glittering.colors_max     = 1;
    Glittering.speed          = DREVO_CALIBUR_V2_SPEED_DEFAULT;
    Glittering.brightness     = DREVO_CALIBUR_V2_BRIGHTNESS_DEFAULT;
    Glittering.color_mode     = MODE_COLORS_RANDOM;
    Glittering.colors.resize(1);
    modes.push_back(Glittering);

    mode Falling;
    Falling.name           = "Falling";
    Falling.value          = DREVO_CALIBUR_V2_MODE_FALLING;
    Falling.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_MODE_SPECIFIC_COLOR | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    Falling.speed_min      = DREVO_CALIBUR_V2_SPEED_MINIMUM;
    Falling.speed_max      = DREVO_CALIBUR_V2_SPEED_MAXIMUM;
    Falling.brightness_min = DREVO_CALIBUR_V2_BRIGHTNESS_MINIMUM;
    Falling.brightness_max = DREVO_CALIBUR_V2_BRIGHTNESS_MAXIMUM;
    Falling.colors_min     = 1;
    Falling.colors_max     = 1;
    Falling.speed          = DREVO_CALIBUR_V2_SPEED_DEFAULT;
    Falling.brightness     = DREVO_CALIBUR_V2_BRIGHTNESS_DEFAULT;
    Falling.color_mode     = MODE_COLORS_RANDOM;
    Falling.colors.resize(1);
    modes.push_back(Falling);

    mode Colourful;
    Colourful.name           = "Colourful";
    Colourful.value          = DREVO_CALIBUR_V2_MODE_COLOURFUL;
    Colourful.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    Colourful.speed_min      = DREVO_CALIBUR_V2_SPEED_MINIMUM;
    Colourful.speed_max      = DREVO_CALIBUR_V2_SPEED_MAXIMUM;
    Colourful.brightness_min = DREVO_CALIBUR_V2_BRIGHTNESS_MINIMUM;
    Colourful.brightness_max = DREVO_CALIBUR_V2_BRIGHTNESS_MAXIMUM;
    Colourful.speed          = DREVO_CALIBUR_V2_SPEED_DEFAULT;
    Colourful.brightness     = DREVO_CALIBUR_V2_BRIGHTNESS_DEFAULT;
    Colourful.color_mode     = MODE_COLORS_RANDOM;
    modes.push_back(Colourful);

    mode Breathing;
    Breathing.name           = "Breathing";
    Breathing.value          = DREVO_CALIBUR_V2_MODE_BREATHING;
    Breathing.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_MODE_SPECIFIC_COLOR | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    Breathing.speed_min      = DREVO_CALIBUR_V2_SPEED_MINIMUM;
    Breathing.speed_max      = DREVO_CALIBUR_V2_SPEED_MAXIMUM;
    Breathing.brightness_min = DREVO_CALIBUR_V2_BRIGHTNESS_MINIMUM;
    Breathing.brightness_max = DREVO_CALIBUR_V2_BRIGHTNESS_MAXIMUM;
    Breathing.colors_min     = 1;
    Breathing.colors_max     = 1;
    Breathing.speed          = DREVO_CALIBUR_V2_SPEED_DEFAULT;
    Breathing.brightness     = DREVO_CALIBUR_V2_BRIGHTNESS_DEFAULT;
    Breathing.color_mode     = MODE_COLORS_RANDOM;
    Breathing.colors.resize(1);
    modes.push_back(Breathing);

    mode SpectrumCycle;
    SpectrumCycle.name           = "Spectrum cycle";
    SpectrumCycle.value          = DREVO_CALIBUR_V2_MODE_SPECTRUM_CYCLE;
    SpectrumCycle.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    SpectrumCycle.speed_min      = DREVO_CALIBUR_V2_SPEED_MINIMUM;
    SpectrumCycle.speed_max      = DREVO_CALIBUR_V2_SPEED_MAXIMUM;
    SpectrumCycle.brightness_min = DREVO_CALIBUR_V2_BRIGHTNESS_MINIMUM;
    SpectrumCycle.brightness_max = DREVO_CALIBUR_V2_BRIGHTNESS_MAXIMUM;
    SpectrumCycle.speed          = DREVO_CALIBUR_V2_SPEED_DEFAULT;
    SpectrumCycle.brightness     = DREVO_CALIBUR_V2_BRIGHTNESS_DEFAULT;
    SpectrumCycle.color_mode     = MODE_COLORS_RANDOM;
    modes.push_back(SpectrumCycle);

    mode Outward;
    Outward.name           = "Outward";
    Outward.value          = DREVO_CALIBUR_V2_MODE_OUTWARD;
    Outward.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_MODE_SPECIFIC_COLOR | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    Outward.speed_min      = DREVO_CALIBUR_V2_SPEED_MINIMUM;
    Outward.speed_max      = DREVO_CALIBUR_V2_SPEED_MAXIMUM;
    Outward.brightness_min = DREVO_CALIBUR_V2_BRIGHTNESS_MINIMUM;
    Outward.brightness_max = DREVO_CALIBUR_V2_BRIGHTNESS_MAXIMUM;
    Outward.colors_min     = 1;
    Outward.colors_max     = 1;
    Outward.speed          = DREVO_CALIBUR_V2_SPEED_DEFAULT;
    Outward.brightness     = DREVO_CALIBUR_V2_BRIGHTNESS_DEFAULT;
    Outward.color_mode     = MODE_COLORS_RANDOM;
    Outward.colors.resize(1);
    modes.push_back(Outward);

    /*--------------------------------------------------------------------------------------*\
    |  Firmware bug: The direction can't be changed in this mode while using random colors.  |
    |  This behavior also happens with the official app.                                     |
    \*--------------------------------------------------------------------------------------*/
    mode Scrolling;
    Scrolling.name           = "Scrolling";
    Scrolling.value          = DREVO_CALIBUR_V2_MODE_SCROLLING;
    Scrolling.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_DIRECTION_UD | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_MODE_SPECIFIC_COLOR | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    Scrolling.speed_min      = DREVO_CALIBUR_V2_SPEED_MINIMUM;
    Scrolling.speed_max      = DREVO_CALIBUR_V2_SPEED_MAXIMUM;
    Scrolling.brightness_min = DREVO_CALIBUR_V2_BRIGHTNESS_MINIMUM;
    Scrolling.brightness_max = DREVO_CALIBUR_V2_BRIGHTNESS_MAXIMUM;
    Scrolling.colors_min     = 1;
    Scrolling.colors_max     = 1;
    Scrolling.speed          = DREVO_CALIBUR_V2_SPEED_DEFAULT;
    Scrolling.brightness     = DREVO_CALIBUR_V2_BRIGHTNESS_DEFAULT;
    Scrolling.direction      = MODE_DIRECTION_DOWN;
    Scrolling.color_mode     = MODE_COLORS_RANDOM;
    Scrolling.colors.resize(1);
    modes.push_back(Scrolling);

    mode Rolling;
    Rolling.name           = "Rolling";
    Rolling.value          = DREVO_CALIBUR_V2_MODE_ROLLING;
    Rolling.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_DIRECTION_LR | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_MODE_SPECIFIC_COLOR | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    Rolling.speed_min      = DREVO_CALIBUR_V2_SPEED_MINIMUM;
    Rolling.speed_max      = DREVO_CALIBUR_V2_SPEED_MAXIMUM;
    Rolling.brightness_min = DREVO_CALIBUR_V2_BRIGHTNESS_MINIMUM;
    Rolling.brightness_max = DREVO_CALIBUR_V2_BRIGHTNESS_MAXIMUM;
    Rolling.colors_min     = 1;
    Rolling.colors_max     = 1;
    Rolling.speed          = DREVO_CALIBUR_V2_SPEED_DEFAULT;
    Rolling.brightness     = DREVO_CALIBUR_V2_BRIGHTNESS_DEFAULT;
    Rolling.direction      = MODE_DIRECTION_RIGHT;
    Rolling.color_mode     = MODE_COLORS_RANDOM;
    Rolling.colors.resize(1);
    modes.push_back(Rolling);

    mode Rotating;
    Rotating.name           = "Rotating";
    Rotating.value          = DREVO_CALIBUR_V2_MODE_ROTATING;
    Rotating.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_MODE_SPECIFIC_COLOR | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    Rotating.speed_min      = DREVO_CALIBUR_V2_SPEED_MINIMUM;
    Rotating.speed_max      = DREVO_CALIBUR_V2_SPEED_MAXIMUM;
    Rotating.brightness_min = DREVO_CALIBUR_V2_BRIGHTNESS_MINIMUM;
    Rotating.brightness_max = DREVO_CALIBUR_V2_BRIGHTNESS_MAXIMUM;
    Rotating.colors_min     = 1;
    Rotating.colors_max     = 1;
    Rotating.speed          = DREVO_CALIBUR_V2_SPEED_DEFAULT;
    Rotating.brightness     = DREVO_CALIBUR_V2_BRIGHTNESS_DEFAULT;
    Rotating.color_mode     = MODE_COLORS_RANDOM;
    Rotating.colors.resize(1);
    modes.push_back(Rotating);

    mode ReactiveExplode;
    ReactiveExplode.name           = "Reactive explode";
    ReactiveExplode.value          = DREVO_CALIBUR_V2_MODE_REACTIVE_EXPLODE;
    ReactiveExplode.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_MODE_SPECIFIC_COLOR | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    ReactiveExplode.speed_min      = DREVO_CALIBUR_V2_SPEED_MINIMUM;
    ReactiveExplode.speed_max      = DREVO_CALIBUR_V2_SPEED_MAXIMUM;
    ReactiveExplode.brightness_min = DREVO_CALIBUR_V2_BRIGHTNESS_MINIMUM;
    ReactiveExplode.brightness_max = DREVO_CALIBUR_V2_BRIGHTNESS_MAXIMUM;
    ReactiveExplode.colors_min     = 1;
    ReactiveExplode.colors_max     = 1;
    ReactiveExplode.speed          = DREVO_CALIBUR_V2_SPEED_DEFAULT;
    ReactiveExplode.brightness     = DREVO_CALIBUR_V2_BRIGHTNESS_DEFAULT;
    ReactiveExplode.color_mode     = MODE_COLORS_RANDOM;
    ReactiveExplode.colors.resize(1);
    modes.push_back(ReactiveExplode);

    mode ReactiveLaunch;
    ReactiveLaunch.name           = "Reactive launch";
    ReactiveLaunch.value          = DREVO_CALIBUR_V2_MODE_REACTIVE_LAUNCH;
    ReactiveLaunch.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_MODE_SPECIFIC_COLOR | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    ReactiveLaunch.speed_min      = DREVO_CALIBUR_V2_SPEED_MINIMUM;
    ReactiveLaunch.speed_max      = DREVO_CALIBUR_V2_SPEED_MAXIMUM;
    ReactiveLaunch.brightness_min = DREVO_CALIBUR_V2_BRIGHTNESS_MINIMUM;
    ReactiveLaunch.brightness_max = DREVO_CALIBUR_V2_BRIGHTNESS_MAXIMUM;
    ReactiveLaunch.colors_min     = 1;
    ReactiveLaunch.colors_max     = 1;
    ReactiveLaunch.speed          = DREVO_CALIBUR_V2_SPEED_DEFAULT;
    ReactiveLaunch.brightness     = DREVO_CALIBUR_V2_BRIGHTNESS_DEFAULT;
    ReactiveLaunch.color_mode     = MODE_COLORS_RANDOM;
    ReactiveLaunch.colors.resize(1);
    modes.push_back(ReactiveLaunch);

    mode ReactiveRipples;
    ReactiveRipples.name           = "Reactive ripples";
    ReactiveRipples.value          = DREVO_CALIBUR_V2_MODE_REACTIVE_RIPPLES;
    ReactiveRipples.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_MODE_SPECIFIC_COLOR | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    ReactiveRipples.speed_min      = DREVO_CALIBUR_V2_SPEED_MINIMUM;
    ReactiveRipples.speed_max      = DREVO_CALIBUR_V2_SPEED_MAXIMUM;
    ReactiveRipples.brightness_min = DREVO_CALIBUR_V2_BRIGHTNESS_MINIMUM;
    ReactiveRipples.brightness_max = DREVO_CALIBUR_V2_BRIGHTNESS_MAXIMUM;
    ReactiveRipples.colors_min     = 1;
    ReactiveRipples.colors_max     = 1;
    ReactiveRipples.speed          = DREVO_CALIBUR_V2_SPEED_DEFAULT;
    ReactiveRipples.brightness     = DREVO_CALIBUR_V2_BRIGHTNESS_DEFAULT;
    ReactiveRipples.color_mode     = MODE_COLORS_RANDOM;
    ReactiveRipples.colors.resize(1);
    modes.push_back(ReactiveRipples);

    mode Flowing;
    Flowing.name           = "Flowing";
    Flowing.value          = DREVO_CALIBUR_V2_MODE_FLOWING;
    Flowing.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_DIRECTION_LR | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_MODE_SPECIFIC_COLOR | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    Flowing.speed_min      = DREVO_CALIBUR_V2_SPEED_MINIMUM;
    Flowing.speed_max      = DREVO_CALIBUR_V2_SPEED_MAXIMUM;
    Flowing.brightness_min = DREVO_CALIBUR_V2_BRIGHTNESS_MINIMUM;
    Flowing.brightness_max = DREVO_CALIBUR_V2_BRIGHTNESS_MAXIMUM;
    Flowing.colors_min     = 1;
    Flowing.colors_max     = 1;
    Flowing.speed          = DREVO_CALIBUR_V2_SPEED_DEFAULT;
    Flowing.brightness     = DREVO_CALIBUR_V2_BRIGHTNESS_DEFAULT;
    Flowing.direction      = MODE_DIRECTION_RIGHT;
    Flowing.color_mode     = MODE_COLORS_RANDOM;
    Flowing.colors.resize(1);
    modes.push_back(Flowing);

    mode Pulsating;
    Pulsating.name           = "Pulsating";
    Pulsating.value          = DREVO_CALIBUR_V2_MODE_PULSATING;
    Pulsating.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_MODE_SPECIFIC_COLOR | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    Pulsating.speed_min      = DREVO_CALIBUR_V2_SPEED_MINIMUM;
    Pulsating.speed_max      = DREVO_CALIBUR_V2_SPEED_MAXIMUM;
    Pulsating.brightness_min = DREVO_CALIBUR_V2_BRIGHTNESS_MINIMUM;
    Pulsating.brightness_max = DREVO_CALIBUR_V2_BRIGHTNESS_MAXIMUM;
    Pulsating.colors_min     = 1;
    Pulsating.colors_max     = 1;
    Pulsating.speed          = DREVO_CALIBUR_V2_SPEED_DEFAULT;
    Pulsating.brightness     = DREVO_CALIBUR_V2_BRIGHTNESS_DEFAULT;
    Pulsating.color_mode     = MODE_COLORS_RANDOM;
    Pulsating.colors.resize(1);
    modes.push_back(Pulsating);

    mode Tilt;
    Tilt.name           = "Tilt";
    Tilt.value          = DREVO_CALIBUR_V2_MODE_TILT;
    Tilt.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_DIRECTION_LR | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_MODE_SPECIFIC_COLOR | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    Tilt.speed_min      = DREVO_CALIBUR_V2_SPEED_MINIMUM;
    Tilt.speed_max      = DREVO_CALIBUR_V2_SPEED_MAXIMUM;
    Tilt.brightness_min = DREVO_CALIBUR_V2_BRIGHTNESS_MINIMUM;
    Tilt.brightness_max = DREVO_CALIBUR_V2_BRIGHTNESS_MAXIMUM;
    Tilt.colors_min     = 1;
    Tilt.colors_max     = 1;
    Tilt.speed          = DREVO_CALIBUR_V2_SPEED_DEFAULT;
    Tilt.brightness     = DREVO_CALIBUR_V2_BRIGHTNESS_DEFAULT;
    Tilt.direction      = MODE_DIRECTION_RIGHT;
    Tilt.color_mode     = MODE_COLORS_RANDOM;
    Tilt.colors.resize(1);
    modes.push_back(Tilt);

    mode Shuttle;
    Shuttle.name           = "Shuttle";
    Shuttle.value          = DREVO_CALIBUR_V2_MODE_SHUTTLE;
    Shuttle.flags          = MODE_FLAG_HAS_SPEED | MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_MODE_SPECIFIC_COLOR | MODE_FLAG_HAS_RANDOM_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    Shuttle.speed_min      = DREVO_CALIBUR_V2_SPEED_MINIMUM;
    Shuttle.speed_max      = DREVO_CALIBUR_V2_SPEED_MAXIMUM;
    Shuttle.brightness_min = DREVO_CALIBUR_V2_BRIGHTNESS_MINIMUM;
    Shuttle.brightness_max = DREVO_CALIBUR_V2_BRIGHTNESS_MAXIMUM;
    Shuttle.colors_min     = 1;
    Shuttle.colors_max     = 1;
    Shuttle.speed          = DREVO_CALIBUR_V2_SPEED_DEFAULT;
    Shuttle.brightness     = DREVO_CALIBUR_V2_BRIGHTNESS_DEFAULT;
    Shuttle.color_mode     = MODE_COLORS_RANDOM;
    Shuttle.colors.resize(1);
    modes.push_back(Shuttle);

    mode Custom;
    Custom.name           = "Custom";
    Custom.value          = DREVO_CALIBUR_V2_MODE_CUSTOM;
    Custom.flags          = MODE_FLAG_HAS_BRIGHTNESS | MODE_FLAG_HAS_PER_LED_COLOR | MODE_FLAG_AUTOMATIC_SAVE;
    Custom.brightness_min = DREVO_CALIBUR_V2_BRIGHTNESS_MINIMUM;
    Custom.brightness_max = DREVO_CALIBUR_V2_BRIGHTNESS_MAXIMUM;
    Custom.brightness     = DREVO_CALIBUR_V2_BRIGHTNESS_DEFAULT;
    Custom.color_mode     = MODE_COLORS_PER_LED;
    modes.push_back(Custom);

    SetupZones();
}

RGBController_DrevoCaliburV2::~RGBController_DrevoCaliburV2()
{
    delete controller;
}

void RGBController_DrevoCaliburV2::SetupZones()
{
    zone new_zone;

    new_zone.name               = "Keyboard";
    new_zone.type               = ZONE_TYPE_MATRIX;
    new_zone.leds_min           = DREVO_CALIBUR_V2_MATRIX_CELL_COUNT;
    new_zone.leds_max           = DREVO_CALIBUR_V2_MATRIX_CELL_COUNT;
    new_zone.leds_count         = DREVO_CALIBUR_V2_MATRIX_CELL_COUNT;
    new_zone.matrix_map         = new matrix_map_type;
    new_zone.matrix_map->height = DREVO_CALIBUR_V2_MATRIX_MAP_HEIGHT;
    new_zone.matrix_map->width  = DREVO_CALIBUR_V2_MATRIX_MAP_WIDTH;
    new_zone.matrix_map->map    = &matrix_map[0][0];

    zones.push_back(new_zone);

    for(int led_idx = 0; led_idx < DREVO_CALIBUR_V2_MATRIX_CELL_COUNT; led_idx++)
    {
        led new_led;

        new_led.name = "Keyboard LED ";
        new_led.name.append(std::to_string(led_idx));

        leds.push_back(new_led);
    }

    SetupColors();
}

void RGBController_DrevoCaliburV2::ResizeZone(int /*zone*/, int /*new_size*/)
{
    /*---------------------------------------------------------*\
    | This device does not support resizing zones               |
    \*---------------------------------------------------------*/
}

void RGBController_DrevoCaliburV2::DeviceUpdateLEDs()
{
    const mode& mode = modes[active_mode];

    unsigned char color_data[DREVO_CALIBUR_V2_CUSTOM_COLORS_LENGTH] = { 0 };

    for(int led_idx = 0; led_idx < DREVO_CALIBUR_V2_NUM_KEYS; led_idx++)
    {
        int data_index = led_mappings[led_idx];
        color_data[(4 * data_index) + 0] = mode.value;
        color_data[(4 * data_index) + 1] = RGBGetRValue(colors[led_idx]);
        color_data[(4 * data_index) + 2] = RGBGetGValue(colors[led_idx]);
        color_data[(4 * data_index) + 3] = RGBGetBValue(colors[led_idx]);
    }

    controller->SetRGBCustom(mode.brightness, color_data);
}

void RGBController_DrevoCaliburV2::UpdateZoneLEDs(int /*zone*/)
{
    DeviceUpdateLEDs();
}

void RGBController_DrevoCaliburV2::UpdateSingleLED(int /*led*/)
{
    DeviceUpdateLEDs();
}

void RGBController_DrevoCaliburV2::DeviceUpdateMode()
{
    const mode& mode = modes[active_mode];
    unsigned char red = 0, green = 0, blue = 0;

    if(!mode.colors.empty())
    {
        red = RGBGetRValue(mode.colors[0]);
        green = RGBGetGValue(mode.colors[0]);
        blue = RGBGetBValue(mode.colors[0]);
    }

    controller->SetRGBMode(mode.value, red, green, blue, mode.color_mode == MODE_COLORS_RANDOM,
                           mode.brightness, mode.speed, mode.direction);
}
