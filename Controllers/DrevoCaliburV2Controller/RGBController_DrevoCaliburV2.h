/*----------------------------------------------*\
|  RGBController_DrevoCaliburV2.h                |
|                                                |
|  Generic RGB Interface for Drevo Calibur V2    |
|                                                |
|  Luiz Henrique Laurini (LHLaurini), 3/02/2024  |
\*----------------------------------------------*/

#pragma once
#include "RGBController.h"
#include "DrevoCaliburV2Controller.h"

class RGBController_DrevoCaliburV2 : public RGBController
{
public:
    RGBController_DrevoCaliburV2(DrevoCaliburV2Controller* controller_ptr);
    ~RGBController_DrevoCaliburV2();

    void    SetupZones();

    void    ResizeZone(int zone, int new_size);

    void    DeviceUpdateLEDs();
    void    UpdateZoneLEDs(int zone);
    void    UpdateSingleLED(int led);

    void    DeviceUpdateMode();

private:
    DrevoCaliburV2Controller* controller;
};
