/*----------------------------------------------*\
|  DrevoCaliburV2ControllerDetect.cpp            |
|                                                |
|  Detector for Drevo Calibur V2                 |
|                                                |
|  Luiz Henrique Laurini (LHLaurini), 3/02/2024  |
\*----------------------------------------------*/

#include "Detector.h"
#include "DrevoCaliburV2Controller.h"
#include "RGBController.h"
#include "RGBController_DrevoCaliburV2.h"
#include "LogManager.h"
#include <vector>
#ifdef __FreeBSD__
#include <libusb.h>
#else
#include <libusb-1.0/libusb.h>
#endif

/*----------------------------------------------------*\
| Apple vendor ID                                      |
\*----------------------------------------------------*/
#define APPLE_VID                       0x05AC

/*----------------------------------------------------*\
| Aluminium Keyboard (ISO) product ID                  |
\*----------------------------------------------------*/
#define ALUMINIUM_KEYBOARD_ISO_PID      0x0250

/*----------------------------------------------------*\
| Sonix Technology Company Limited manufacturer string |
\*----------------------------------------------------*/

#define SONIX_MANUFACTURER_STRING "SONiX"

/*----------------------------------------------------*\
| Calibur V2 product string prefix                     |
\*----------------------------------------------------*/

#define CALIBUR_V2_PRO_PRODUCT_STRING "Calibur V2 PRO"

/*************************************************************************************\
*                                                                                     *
*   DetectDrevoCaliburV2Controllers                                                   *
*                                                                                     *
*       Tests the USB address to see if a Drevo Calibur V2 controller exists there.   *
*                                                                                     *
\*************************************************************************************/

void DetectDrevoCaliburV2Controllers()
{
    libusb_init(NULL);

    libusb_device** device_list;
    ssize_t num_devices = libusb_get_device_list(NULL, &device_list);

    if(num_devices < 0)
    {
        return;
    }

    LOG_TRACE("Got device list");

    for(int device_idx = 0; device_idx < num_devices; device_idx++)
    {
        libusb_device* device = device_list[device_idx];

        LOG_TRACE("Getting descriptor");

        libusb_device_descriptor descriptor;
        if(libusb_get_device_descriptor(device, &descriptor) < 0 ||
           (descriptor.idVendor != APPLE_VID || descriptor.idProduct != ALUMINIUM_KEYBOARD_ISO_PID))
        {
            continue;
        }

        LOG_TRACE("Got descriptor");

        /*------------------------------------------------------------------*\
        | The Drevo Calibur V2 keyboard uses Apple's VID and PID, so we also |
        | check the manufacturer and product strings.                        |
        \*------------------------------------------------------------------*/

        libusb_device_handle* device_handle;

        if(libusb_open(device, &device_handle) < 0)
        {
            LOG_TRACE("Failed to open");
            continue;
        }

        if(!DrevoCaliburV2Controller::Claim(device_handle))
        {
            LOG_TRACE("Failed to claim");
            continue;
        }

        LOG_TRACE("...");

        unsigned char buffer[32];
        std::string manufacturer_string;
        std::string product_string;
        int num_bytes;

        num_bytes = libusb_get_string_descriptor_ascii(device_handle, descriptor.iManufacturer, buffer, sizeof buffer);
        if(num_bytes > 0)
        {
            manufacturer_string.assign((char*)buffer, num_bytes);
        }

        num_bytes = libusb_get_string_descriptor_ascii(device_handle, descriptor.iProduct, buffer, sizeof buffer);
        if(num_bytes > 0)
        {
            product_string.assign((char*)buffer, num_bytes);
        }

        LOG_TRACE("Manufacturer: %s", manufacturer_string.c_str());
        LOG_TRACE("Product: %s", product_string.c_str());

        DrevoCaliburV2Controller::Release(device_handle);

        if(manufacturer_string == SONIX_MANUFACTURER_STRING &&
           product_string == CALIBUR_V2_PRO_PRODUCT_STRING)
        {
            DrevoCaliburV2Controller*       controller     = new DrevoCaliburV2Controller(device_handle);
            RGBController_DrevoCaliburV2*   rgb_controller = new RGBController_DrevoCaliburV2(controller);
            rgb_controller->name                           = "Drevo " + product_string;

            ResourceManager::get()->RegisterRGBController(rgb_controller);
        }
        else
        {
            libusb_close(device_handle);
        }
    }

    libusb_free_device_list(device_list, 1);
}   /* DetectDrevoCaliburV2Controllers() */

REGISTER_DETECTOR("Drevo Calibur V2 Pro", DetectDrevoCaliburV2Controllers);
