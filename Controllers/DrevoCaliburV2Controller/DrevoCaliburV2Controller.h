/*----------------------------------------------*\
|  DrevoCaliburV2Controller.h                    |
|                                                |
|  Definitions for Drevo Calibur V2              |
|                                                |
|  Luiz Henrique Laurini (LHLaurini), 3/02/2024  |
\*----------------------------------------------*/

#include "RGBController.h"

#pragma once

struct libusb_device_handle;

enum
{
    DREVO_CALIBUR_V2_SPEED_MINIMUM = 0,
    DREVO_CALIBUR_V2_SPEED_DEFAULT = 7,
    DREVO_CALIBUR_V2_SPEED_MAXIMUM = 15,
};

enum
{
    DREVO_CALIBUR_V2_BRIGHTNESS_MINIMUM = 0,
    DREVO_CALIBUR_V2_BRIGHTNESS_DEFAULT = 15,
    DREVO_CALIBUR_V2_BRIGHTNESS_MAXIMUM = 15,
};

enum
{
    /*--------------------------------*\
    |  Original name: Static           |
    \*_-------------------------------*/
    DREVO_CALIBUR_V2_MODE_STATIC = 1,
    /*--------------------------------*\
    |  Original name: SingleOn         |
    \*_-------------------------------*/
    DREVO_CALIBUR_V2_MODE_REACTIVE,
    /*--------------------------------*\
    |  Original name: SingleOff        |
    \*_-------------------------------*/
    DREVO_CALIBUR_V2_MODE_REACTIVE_OFF,
    /*--------------------------------*\
    |  Original name: Glittering       |
    \*_-------------------------------*/
    DREVO_CALIBUR_V2_MODE_GLITTERING,
    /*--------------------------------*\
    |  Original name: Falling          |
    \*_-------------------------------*/
    DREVO_CALIBUR_V2_MODE_FALLING,
    /*--------------------------------*\
    |  Original name: Colourful        |
    \*_-------------------------------*/
    DREVO_CALIBUR_V2_MODE_COLOURFUL,
    /*--------------------------------*\
    |  Original name: Breath           |
    \*_-------------------------------*/
    DREVO_CALIBUR_V2_MODE_BREATHING,
    /*--------------------------------*\
    |  Original name: Spectrum         |
    \*_-------------------------------*/
    DREVO_CALIBUR_V2_MODE_SPECTRUM_CYCLE,
    /*--------------------------------*\
    |  Original name: Outward          |
    \*_-------------------------------*/
    DREVO_CALIBUR_V2_MODE_OUTWARD,
    /*--------------------------------*\
    |  Original name: Scrolling        |
    \*_-------------------------------*/
    DREVO_CALIBUR_V2_MODE_SCROLLING,
    /*--------------------------------*\
    |  Original name: Rolling          |
    \*_-------------------------------*/
    DREVO_CALIBUR_V2_MODE_ROLLING,
    /*--------------------------------*\
    |  Original name: Rotating         |
    \*_-------------------------------*/
    DREVO_CALIBUR_V2_MODE_ROTATING,
    /*--------------------------------*\
    |  Original name: Explode          |
    \*_-------------------------------*/
    DREVO_CALIBUR_V2_MODE_REACTIVE_EXPLODE,
    /*--------------------------------*\
    |  Original name: Launch           |
    \*_-------------------------------*/
    DREVO_CALIBUR_V2_MODE_REACTIVE_LAUNCH,
    /*--------------------------------*\
    |  Original name: Ripples          |
    \*_-------------------------------*/
    DREVO_CALIBUR_V2_MODE_REACTIVE_RIPPLES,
    /*--------------------------------*\
    |  Original name: Flowing          |
    \*_-------------------------------*/
    DREVO_CALIBUR_V2_MODE_FLOWING,
    /*--------------------------------*\
    |  Original name: Pulsating        |
    \*_-------------------------------*/
    DREVO_CALIBUR_V2_MODE_PULSATING,
    /*--------------------------------*\
    |  Original name: Tilt             |
    \*_-------------------------------*/
    DREVO_CALIBUR_V2_MODE_TILT,
    /*--------------------------------*\
    |  Original name: Shuttle          |
    \*_-------------------------------*/
    DREVO_CALIBUR_V2_MODE_SHUTTLE,
    /*--------------------------------*\
    |  Original name: UserDefine       |
    \*_-------------------------------*/
    DREVO_CALIBUR_V2_MODE_CUSTOM = 0x80,
};

#define DREVO_CALIBUR_V2_CUSTOM_COLORS_LENGTH (4 * 144)
#define DREVO_CALIBUR_V2_NUM_MODES 20

static const int drevo_calibur_v2_modes[DREVO_CALIBUR_V2_NUM_MODES] = {
    DREVO_CALIBUR_V2_MODE_STATIC,
    DREVO_CALIBUR_V2_MODE_REACTIVE,
    DREVO_CALIBUR_V2_MODE_REACTIVE_OFF,
    DREVO_CALIBUR_V2_MODE_GLITTERING,
    DREVO_CALIBUR_V2_MODE_FALLING,
    DREVO_CALIBUR_V2_MODE_COLOURFUL,
    DREVO_CALIBUR_V2_MODE_BREATHING,
    DREVO_CALIBUR_V2_MODE_SPECTRUM_CYCLE,
    DREVO_CALIBUR_V2_MODE_OUTWARD,
    DREVO_CALIBUR_V2_MODE_SCROLLING,
    DREVO_CALIBUR_V2_MODE_ROLLING,
    DREVO_CALIBUR_V2_MODE_ROTATING,
    DREVO_CALIBUR_V2_MODE_REACTIVE_EXPLODE,
    DREVO_CALIBUR_V2_MODE_REACTIVE_LAUNCH,
    DREVO_CALIBUR_V2_MODE_REACTIVE_RIPPLES,
    DREVO_CALIBUR_V2_MODE_FLOWING,
    DREVO_CALIBUR_V2_MODE_PULSATING,
    DREVO_CALIBUR_V2_MODE_TILT,
    DREVO_CALIBUR_V2_MODE_SHUTTLE,
    DREVO_CALIBUR_V2_MODE_CUSTOM,
};

class DrevoCaliburV2Controller
{
public:
    DrevoCaliburV2Controller(libusb_device_handle* dev_handle);
    ~DrevoCaliburV2Controller();

    std::string GetLocation();

    void SetRGBMode(int mode, unsigned char red, unsigned char green, unsigned char blue, int random,
                    int brightness, int speed, int direction);
    void SetRGBCustom(int brightness, unsigned char* colors);

    static bool Claim(libusb_device_handle* dev);
    static void Release(libusb_device_handle* dev);

private:
    libusb_device_handle*   dev;
    std::string             location;

    void SetRGB(int mode, unsigned char red, unsigned char green, unsigned char blue, bool random,
                int brightness, int speed, int direction, unsigned char* colors);

    bool Claim();
    void Release();

    static int ConvertDirection(int direction);

    bool Communicate(libusb_device_handle* dev, uint8_t request_type, uint8_t request,
                     uint16_t value, uint16_t index, unsigned char* data, uint16_t num_transfers);
    bool Receive(unsigned char* data, uint16_t num_transfers);
    bool Send(unsigned char* data, uint16_t num_transfers);
};
