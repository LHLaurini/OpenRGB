/*----------------------------------------------*\
|  DrevoCaliburV2Controller.h                    |
|                                                |
|  Processing Code for Drevo Calibur V2          |
|                                                |
|  Luiz Henrique Laurini (LHLaurini), 3/03/2024  |
\*----------------------------------------------*/

#include "DrevoCaliburV2Controller.h"

#include "LogManager.h"
#include <sstream>
#ifdef __FreeBSD__
#include <libusb.h>
#else
#include <libusb-1.0/libusb.h>
#endif

#define DREVO_CALIBUR_V2_RECEIVE_REQUEST_TYPE   LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_INTERFACE
#define DREVO_CALIBUR_V2_RECEIVE_REQUEST        1
#define DREVO_CALIBUR_V2_RECEIVE_VALUE          0x0300
#define DREVO_CALIBUR_V2_RECEIVE_INDEX          0

#define DREVO_CALIBUR_V2_SEND_REQUEST_TYPE  LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_INTERFACE
#define DREVO_CALIBUR_V2_SEND_REQUEST       9
#define DREVO_CALIBUR_V2_SEND_VALUE         0x0300
#define DREVO_CALIBUR_V2_SEND_INDEX         0

#define DREVO_CALIBUR_V2_TRANSFER_LENGTH 64

DrevoCaliburV2Controller::DrevoCaliburV2Controller(libusb_device_handle* dev_handle)
{
    dev = dev_handle;

    /*---------------------------------------------*\
    | Fill in location string with USB port numbers |
    \*---------------------------------------------*/

    uint8_t port_numbers[7];

    int num_port_numbers = libusb_get_port_numbers(libusb_get_device(dev), port_numbers, sizeof port_numbers);

    std::ostringstream stream;
    stream << "USB: " << (int)libusb_get_bus_number(libusb_get_device(dev));

    for(int port_number_idx = 0; port_number_idx < num_port_numbers; port_number_idx++)
    {
        stream << (port_number_idx == 0 ? ":" : ".") << (int)port_numbers[port_number_idx];
    }

    location = stream.str();
}

DrevoCaliburV2Controller::~DrevoCaliburV2Controller()
{
    if(dev)
    {
        libusb_close(dev);
    }
}

std::string DrevoCaliburV2Controller::GetLocation()
{
    return(location);
}

void DrevoCaliburV2Controller::SetRGBMode(int mode, unsigned char red, unsigned char green, unsigned char blue, int random,
                int brightness, int speed, int direction)
{
    SetRGB(mode, red, green, blue, random, brightness, speed, direction, NULL);
}

void DrevoCaliburV2Controller::SetRGBCustom(int brightness, unsigned char* colors)
{
    SetRGB(DREVO_CALIBUR_V2_MODE_CUSTOM, 0, 0, 0, 0, brightness, 0, 0, colors);
}

bool DrevoCaliburV2Controller::Claim(libusb_device_handle* dev)
{
    switch(libusb_detach_kernel_driver(dev, 0))
    {
        case 0:
        case LIBUSB_ERROR_NOT_FOUND:
        case LIBUSB_ERROR_NOT_SUPPORTED:
            break;

        default:
            return(false);
    }

    if(libusb_claim_interface(dev, 0) != 0)
    {
        return(false);
    }

    return(true);
}

void DrevoCaliburV2Controller::Release(libusb_device_handle* dev)
{
    libusb_release_interface(dev, 0);
    libusb_attach_kernel_driver(dev, 0);
}

void DrevoCaliburV2Controller::SetRGB(int mode, unsigned char red, unsigned char green, unsigned char blue, bool random,
                                      int brightness, int speed, int direction, unsigned char* colors)
{
    Claim();

    unsigned char buffer[64 * 18];

    memset(buffer, 0, DREVO_CALIBUR_V2_TRANSFER_LENGTH);
    buffer[0] = 0x04;
    buffer[1] = 0x18;

    if(!(Send(buffer, 1) && Receive(buffer, 1)))
    {
        return;
    }

    LOG_TRACE("Response from first command: %02X", buffer[3]);

    memset(buffer, 0, DREVO_CALIBUR_V2_TRANSFER_LENGTH);
    buffer[0] = 0x04;
    buffer[1] = 0x13;
    buffer[8] = 0x12;

    if(!(Send(buffer, 1) && Receive(buffer, 1)))
    {
        return;
    }

    LOG_TRACE("Response from second command: %02X", buffer[3]);

    memset(buffer, 0, DREVO_CALIBUR_V2_TRANSFER_LENGTH * 18);

    for(int mode_idx = 0; mode_idx < DREVO_CALIBUR_V2_NUM_MODES; mode_idx++)
    {
        buffer[16 * mode_idx + 0] = mode_idx;
        buffer[16 * mode_idx + 1] = red;
        buffer[16 * mode_idx + 2] = green;
        buffer[16 * mode_idx + 3] = blue;
        buffer[16 * mode_idx + 8] = random;
        buffer[16 * mode_idx + 9] = brightness;
        buffer[16 * mode_idx + 10] = speed;
        buffer[16 * mode_idx + 11] = ConvertDirection(direction);
        buffer[16 * mode_idx + 14] = 0xAA;
        buffer[16 * mode_idx + 15] = 0x55;
    }

    if(colors)
    {
        memcpy(&buffer[0x200], colors, DREVO_CALIBUR_V2_CUSTOM_COLORS_LENGTH);
    }

    buffer[0x440 + 0] = mode;
    buffer[0x440 + 1] = red;
    buffer[0x440 + 2] = green;
    buffer[0x440 + 3] = blue;
    buffer[0x440 + 8] = random;
    buffer[0x440 + 9] = brightness;
    buffer[0x440 + 10] = speed;
    buffer[0x440 + 11] = ConvertDirection(direction);
    buffer[0x440 + 14] = 0xAA;
    buffer[0x440 + 15] = 0x55;

    if (!Send(buffer, 18))
    {
        return;
    }

    memset(buffer, 0, DREVO_CALIBUR_V2_TRANSFER_LENGTH);
    buffer[0] = 0x04;
    buffer[1] = 0x02;

    if(!(Send(buffer, 1) && Receive(buffer, 1)))
    {
        return;
    }

    LOG_TRACE("Response from third command: %02X %02X %02X", buffer[3], buffer[4], buffer[5]);

    memset(buffer, 0, DREVO_CALIBUR_V2_TRANSFER_LENGTH);
    buffer[0] = 0x04;
    buffer[1] = 0xf0;

    if(!(Send(buffer, 1) && Receive(buffer, 1)))
    {
        return;
    }

    LOG_TRACE("Response from fourth command: %02X", buffer[3]);

    Release();
}

bool DrevoCaliburV2Controller::Claim()
{
    return(Claim(dev));
}

void DrevoCaliburV2Controller::Release()
{
    return(Release(dev));
}

int DrevoCaliburV2Controller::ConvertDirection(int direction)
{
    switch(direction)
    {
        case MODE_DIRECTION_RIGHT:
            return(0);

        case MODE_DIRECTION_LEFT:
            return(1);

        case MODE_DIRECTION_UP:
            return(2);

        case MODE_DIRECTION_DOWN:
            return(3);

        default:
            LOG_WARNING("unexpected direction value");
            return(0);
    }
}

bool DrevoCaliburV2Controller::Communicate(libusb_device_handle* dev, uint8_t request_type,
                                           uint8_t request, uint16_t value, uint16_t index,
                                           unsigned char* data, uint16_t num_transfers)
{
    for(int transfer_idx = 0; transfer_idx < num_transfers; transfer_idx++)
    {
        if(libusb_control_transfer(dev, request_type, request, value, index,
                                   data + DREVO_CALIBUR_V2_TRANSFER_LENGTH * transfer_idx,
                                   DREVO_CALIBUR_V2_TRANSFER_LENGTH, 0) < 0)
        {
            return(false);
        }
    }
    return(true);
}

bool DrevoCaliburV2Controller::Receive(unsigned char* data, uint16_t num_transfers)
{
    return(Communicate(dev, DREVO_CALIBUR_V2_RECEIVE_REQUEST_TYPE, DREVO_CALIBUR_V2_RECEIVE_REQUEST,
                       DREVO_CALIBUR_V2_RECEIVE_VALUE, DREVO_CALIBUR_V2_RECEIVE_INDEX, data,
                       num_transfers));
}

bool DrevoCaliburV2Controller::Send(unsigned char* data, uint16_t num_transfers)
{
    return(Communicate(dev, DREVO_CALIBUR_V2_SEND_REQUEST_TYPE, DREVO_CALIBUR_V2_SEND_REQUEST,
                       DREVO_CALIBUR_V2_SEND_VALUE, DREVO_CALIBUR_V2_SEND_INDEX, data,
                       num_transfers));
}
